import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

n_total = 0

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter URL:')
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html,'html.parser')

# Retrieve all of the span tags
tags = soup('span')
for tag in tags:
    n_total = n_total + int(tag.contents[0])

print(n_total)
