def chop(list):  # does modify the list
    list.pop(0)
    list.pop(len(list)-1)

def middle(list):  # does not modify the list, thus a list must be returned
    return list[1:-1]

# var initialisation
letters = ['a','b','c','d','e']

print(middle(letters))  # middle must be put inside the print function so the returned value will be printed
chop(letters)
print(letters)  # letters is now modified