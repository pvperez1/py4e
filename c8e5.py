# var initialisations
count = 0

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    words = line.split()
    #print ('Debug:', words)
    if (len(words) == 0) or (words[0] != 'From') : continue   # skips for empty lists and lines not starting with 'From'
    if len(words) >= 2 :
        count += 1
        print(words[1]) # prints only when there is at least 2 elements in the list

print("There were",count,"lines in the file with From as the first word")