# var initialisations
words = []

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    line = line.split()
    for word in line:
        if word not in words:   # if word is not yet in the list
            words.append(word)      # then add it

words.sort()    # sort the words in the list
print(words)