#ask user
fname = input("Enter a file name: ")

#try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ",fname)
    quit()

#loop through file
for line in fhandle:
    #remove newline and capitalise all letters
    print(line.strip('\n').upper())
