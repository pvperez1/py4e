# var initialisations
emails = {}

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    words = line.split()
    # print ('Debug:', words)
    if (len(words) == 0) or (words[0] != 'From'): continue  # skips for empty lists and lines not starting with 'From'
    if len(words) >= 3:
        emails[words[2]] = emails.get(words[2],0) + 1 # saves the 'day' as key if not yet assigned, otherwise adds 1

print(emails)