# var initialisations
num = []
input_num = ""

while True:
    input_num = input("Enter a number: ")

    if input_num == "done":
        break  # exit out of loop

    try:
        input_num = float(input_num)
    except:
        print("Invalid input")
        continue  # skip to next number

    num.append(input_num)  # store number

if len(num) > 0: # protection from empty lists
    print("Maximum:", max(num))
    print("Minimum:", min(num))
