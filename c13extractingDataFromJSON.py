import urllib.request, urllib.error
import json

sum_comments = 0

json_loc = input('Enter location:')

json_data = ''

fhand = urllib.request.urlopen(json_loc)

# get json data
for line in fhand:
    json_data = json_data + line.decode()

# use json
json_data = json.loads(json_data)

# get count sum
for item in json_data["comments"]:
    sum_comments = sum_comments + int(item["count"])

print(sum_comments)