import re
# var initialisations
re_count = 0
re_input = ""

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

re_input = input("Enter a regular expression:")

# loop through file
for line in fhandle:
    x = re.findall(re_input,line)
    if len(x) > 0:
        re_count += 1

print(fname,"had",re_count,"lines that matched",re_input)
