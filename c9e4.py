# var initialisations
emails = {}
max = None
max_email = ""

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    words = line.split()
    if (len(words) == 0) or (words[0] != 'From'): continue  # skips for empty lists and lines not starting with 'From'
    if len(words) >= 3:
        emails[words[1]] = emails.get(words[1],0) + 1 # saves the 'email' as key if not yet assigned, otherwise adds 1

# loop through the dict
for key,value in emails.items():
    if (max is None) or (max<value): # if max is unassigned or greater value is found
        max = value                 # assign value as max
        max_email = key             # assign key as max_email

print(max_email,max)