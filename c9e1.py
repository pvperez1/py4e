# var initialisations
words = {}

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    line = line.split()
    for key in line:
        words[key] = 1

while True: # let user enter test words
    word = input("Enter a word or type 'done':")
    if word == 'done': exit()
    if word in words:  #  check if word is in the dict
        print("The word '",word,"' is in the file")
    else:
        print("The word '",word,"' is not found")
