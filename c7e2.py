# var initialisations
sum = 0
count = 0
ave = 0

# ask user
fname = input("Enter a file name: ")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    if line.find('X-DSPAM-Confidence') != -1:  # this means true if string is found
        sum = sum + float(line[line.find(':')+1:])  # splice line starting after ':' then convert to float
        count += 1

# compute for ave with division by zero check
if count > 0:
    ave = sum / count

print("Average spam confidence:", ave)

