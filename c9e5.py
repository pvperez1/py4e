# var initialisations
emails = {}

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    words = line.split()
    if (len(words) == 0) or (words[0] != 'From'): continue  # skips for empty lists and lines not starting with 'From'
    if len(words) >= 3:
        domain = words[1].split('@') # split to separate username from domain
        emails[domain[1]] = emails.get(domain[1],0) + 1 # saves the 'domain' as key if not yet assigned, otherwise adds 1

print(emails)