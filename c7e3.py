# var initialisations
count = 0

# ask user
fname = input("Enter a file name: ")

# easter egg
if fname == 'na na boo boo':
    print("NA NA BOO BOO TO YOU - You have been punk'd!")
else:
    # try opening file
    try:
        fhandle = open(fname)
    except:
        print("File cannot be opened:", fname)
        quit()

    # loop through file
    for line in fhandle:
        if line.startswith('Subject:'):
            count += 1

    print('There were', count, 'subject lines in', fname)