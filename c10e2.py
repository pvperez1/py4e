# var initialisations
emails = {}

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    words = line.split()
    # skip for lines with less than 6 words and lines not starting with 'From'
    if (len(words) < 6) or (words[0] != 'From'): continue
    # split to separate time elements
    time = words[5].split(':')
    # save the 'time' as key if not yet assigned, otherwise adds 1
    emails[time[0]] = emails.get(time[0],0) + 1

# the list comprehension returns the sorted elements from the list
for e_hour,e_count in sorted([(hour,count) for (hour,count) in emails.items()]):
    print(e_hour,e_count)