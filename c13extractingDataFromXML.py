import urllib.request, urllib.error
import xml.etree.ElementTree as ET

sum_comments = 0

xml_loc = input('Enter location:')

xml_data = ''

fhand = urllib.request.urlopen(xml_loc)

# get xml data
for line in fhand:
    xml_data = xml_data + line.decode()

# use XML Tree
xml_data = ET.fromstring(xml_data)
comments = xml_data.findall('.//count')

# get count sum
for comment in comments:
    sum_comments = sum_comments + int(comment.text)

print(sum_comments)