import re
# var initialisations
re_count = 0
re_sum = 0
re_ave = 0

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()


# loop through file
for line in fhandle:
    line = line.rstrip()
    x = re.findall('^New Revision: ([0-9]+)',line)
    if len(x) > 0:
        print(x)
        re_count += 1
        re_sum = re_sum + float(x[0])

if re_count > 0:
    print(re_sum/re_count)
