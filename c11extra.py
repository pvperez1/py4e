import re
# var initialisations
x = []
n_total = 0

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()


# loop through file
for line in fhandle:
    line = line.rstrip()
    temp = re.findall('[0-9]+',line)
    if len(temp) > 0:
        x = x + temp

for n in x:
    n_total = n_total + int(n)

print(n_total)