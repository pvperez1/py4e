sum = 0
ave = None
count = 0

while True:
    num = input("Enter a number: ")

    if num == "done":
        break  # exit out of loop

    try:
        num = int(num)
    except:
        print("Invalid input")
        continue  # skip to next number

    sum += num
    count += 1

if count > 0: # to avoid division by zero
    ave = sum / count

print(sum, count, ave)

