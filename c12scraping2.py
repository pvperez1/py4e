import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter URL:')
count = int(input('Enter count:'))
pos = int(input('Enter position:'))
print('Retrieving:',url)
while count>0:
    html = urllib.request.urlopen(url, context=ctx).read()
    soup = BeautifulSoup(html,'html.parser')

    # Retrieve all of the a tags
    tags = soup('a')
    url = tags[pos-1].get('href')
    print('Retrieving:', url)
    count -= 1
