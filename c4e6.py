def computepay(hours, rate):
    if hours > 40 :
        pay = (40 * rate) + (hours - 40)*rate*1.5
        return pay
    else :
        pay = hours * rate
        return pay

try:
    hours = float(input('Enter hours: '))
except:
    print('Error, please enter numeric input')
    exit()
try:
    rate = float(input('Enter rate: '))
except:
    print('Error, please enter numeric input')
    exit()

pay = computepay(hours, rate)
print('Pay: ', pay)
