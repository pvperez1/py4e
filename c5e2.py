sum = 0
max = None
min = None
count = 0

while True:
    num = input("Enter a number: ")

    if num == "done":
        break  # exit out of loop

    try:
        num = int(num)
    except:
        print("Invalid input")
        continue  # skip to next number

    sum += num
    count += 1

    # max and min should be None on first iteration
    if max is None and min is None:
        max = num
        min = num

    # get max number
    if max < num:
        max = num

    # get min number
    if min > num:
        min = num

print(sum, count, ", Max number is: ", max, ", Min number is: ", min)
