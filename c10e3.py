# var initialisations
letters = {}

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    # convert each line to all lowercase
    line = line.lower()
    # loop through each char within the line
    for char in line:
        if char.isalpha(): # discards non-alpha characters
            letters[char] = letters.get(char,0) + 1

# the list comprehension returns the sorted elements in reverse order from the list
for num,c in sorted([(count,char) for (char,count) in letters.items()], reverse=True):
    print(c,num)