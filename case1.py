# var initialisations
sum = 0
fields = ""

# ask user
fname = input("Enter a file name: ")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    fields = line.split('|')
    if fields[1] == "Central":  # check if Central district
        sum = sum + int(fields[2])  # sum Approvals for 2016

print("Total of Approvals for 2016 for Central district:", sum)