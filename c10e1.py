# var initialisations
emails = {}

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    words = line.split()
    # skip for lines with less than 3 words and lines not starting with 'From'
    if (len(words) < 3) or (words[0] != 'From'): continue
    # save the 'email' as key if not yet assigned, otherwise adds 1
    emails[words[1]] = emails.get(words[1],0) + 1

# the list comprehension returns the first element in the list in the form [(max_commits, max_email)]
for m_commit,m_email in sorted([(commit,email) for (email,commit) in emails.items()], reverse=True)[0:1]:
    print(m_email,m_commit)