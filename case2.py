# var initialisations
sum_Weighted_Trips = 0
lines_written = 0
max_Weighted_Trips = None
min_Weighted_Trips = None
max_LGA = ""
min_LGA = ""
valid_mode = ["Bus","Train","Vehicle","Walk"]
fields = ""
out_line = ""

# ask user
fname = input("Enter a file name: ")

# try opening file
try:
    fileinput = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# get mode of transpo
while True:
    mode = input("Choose mode of transportation (Bus, Train, Vehicle, Walk):")
    if mode in valid_mode:
        break
    else:
        print("Please choose among the following only (Bus, Train, Vehicle, Walk):")

# create output file
fileoutput = open('Case2_output.txt', 'w')
# write headers for output
fileoutput.write("LGA,Mode,Weighted Trips\n")

# loop through file
for line in fileinput:
    fields = line.split(',')
    if fields[5].find(mode) != -1:  # check if row has mode
        sum_Weighted_Trips = sum_Weighted_Trips + int(fields[6])  # sum of Weighted Trips for 'mode'
        if max_Weighted_Trips is None or max_Weighted_Trips < int(fields[6]): # get max Weighted Trips and for which LGA
            max_Weighted_Trips = int(fields[6])
            max_LGA = fields[4]
        if min_Weighted_Trips is None or min_Weighted_Trips > int(fields[6]): # get min Weighted Trips and for which LGA
            min_Weighted_Trips = int(fields[6])
            min_LGA = fields[4]
        out_line = fields[4]+","+mode+","+fields[6]+"\n" # concatenate data to be written
        fileoutput.write(out_line) # write data to file
        lines_written += 1

# write totals to last line of file
fileoutput.write("-----------------------------------------\n")
out_line = "Total Weighted Trips for "+mode+" is "+str(sum_Weighted_Trips)+"\n"
fileoutput.write(out_line)
out_line = "Max Weighted Trips is "+str(max_Weighted_Trips)+" and it is from "+max_LGA+"\n"
fileoutput.write(out_line)
out_line = "Min Weighted Trips is "+str(min_Weighted_Trips)+" and it is from "+min_LGA+"\n"
fileoutput.write(out_line)

# close files
fileinput.close()
fileoutput.close()
print("Total records written:",lines_written)
