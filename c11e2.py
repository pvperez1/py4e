import re
# var initialisations
re_count = 0

# ask user
fname = input("Enter file:")

# try opening file
try:
    fhandle = open(fname)
except:
    print("Error in opening file: ", fname)
    quit()

# loop through file
for line in fhandle:
    x = re.findall('^New Revision: [0-9]+',line)
    if len(x) > 0:
        print(x)
        re_count += 1

print("Count is:",re_count)